<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<link rel='stylesheet' type='text/css' href='/ThesisProject/resources/css/style.css'>
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet" type="text/css"/>
	<script language="javascript" type="text/javascript" src="/ThesisProject/resources/js/jquery.js"></script>
	<title>Places</title>
	
</head>

<body>
	<div id="wrapper">
    	<div id="content">
    	<h2>Recommended Places According to User Preferences</h2>
			<c:forEach var="entry" items="${recommendedPlaces}">
    			<b>${entry.key}</b><br/> 
    			<c:forEach items="${entry.value}" var="places">
    				${places}<br/>
    			</c:forEach>			
			</c:forEach>
		</div>
	</div>	
</body>

</html>
