package de.tum.in.aics.thesis.project.controllers;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;






import de.tum.in.aics.thesis.project.models.Location;
import de.tum.in.aics.thesis.project.models.Place;
import de.tum.in.aics.thesis.project.services.interfaces.IPlacesService;
import de.tum.in.aics.thesis.project.services.interfaces.ISearchService;
import de.tum.in.aics.thesis.project.services.interfaces.IUserService;

@Controller
public class PlacesController {

	private static double SOURCE_LATITUDE = 0;
	private static double SOURCE_LONGITUDE = 0;
	private static double DEST_LATITUDE = 0;
	private static double DEST_LONGITUDE = 0;
	
	@Autowired
	private IPlacesService placesService;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	ISearchService searchService ;

	
	@RequestMapping(method = RequestMethod.GET, value = "/location")
	public ModelAndView getLocation(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("location");
		return model; 
	}
		
	@RequestMapping(method = RequestMethod.POST, value = "/user/location")
    public @ResponseBody String userLocations(HttpServletRequest request, @RequestParam(value = "sourceLat" ) double sourceLat, @RequestParam(value = "sourceLng" ) double sourceLng, @RequestParam(value = "destinationLat" ) double destinationLat, @RequestParam(value = "destinationLng" ) double destinationLng) {
		SOURCE_LATITUDE = sourceLat;
		SOURCE_LONGITUDE = sourceLng;
		DEST_LATITUDE = destinationLat;
		DEST_LONGITUDE = destinationLng;
		List<Place> places = searchService.explore(new Location(SOURCE_LATITUDE,SOURCE_LONGITUDE), new Location(DEST_LATITUDE,DEST_LONGITUDE));		
		
		return "userpreferences";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/places")
	public ModelAndView showRecommnededPlaces(HttpServletRequest request) {
		
		Map< String, Integer> userPreferences = new HashMap< String, Integer>();
		Map< String, Integer> sortedUserPreferences = new HashMap< String, Integer>();
		Map< String, Map< String, Integer >> categorisedPlaces = new HashMap< String, Map< String, Integer >>();
		Map<String , List<String>> recommendedPlaces = new LinkedHashMap<String, List<String>>();
		
		userPreferences.put("Museum", Integer.parseInt(request.getParameter("museum")));
		userPreferences.put("Night Life", Integer.parseInt(request.getParameter("nightlife")));
		userPreferences.put("Food", Integer.parseInt(request.getParameter("food")));
		userPreferences.put("Art", Integer.parseInt(request.getParameter("art")));
		userPreferences.put("Nature", Integer.parseInt(request.getParameter("nature")));
		userPreferences.put("Music", Integer.parseInt(request.getParameter("music")));
		userPreferences.put("Shopping", Integer.parseInt(request.getParameter("shopping")));
		userPreferences.put("Sports", Integer.parseInt(request.getParameter("sports")));
		
		userService.scalePreferences(userPreferences);	
		sortedUserPreferences = userService.sortPreferences(userPreferences);			
		List<Place> places = searchService.search(new Location(SOURCE_LATITUDE,SOURCE_LONGITUDE), new Location(DEST_LATITUDE,DEST_LONGITUDE));		
		categorisedPlaces = placesService.categorize(places);
		recommendedPlaces = placesService.getRecommendedPlaces(sortedUserPreferences, categorisedPlaces);

		ModelAndView model = new ModelAndView("places");
		model.addObject("recommendedPlaces", recommendedPlaces);
		return model;
	}
	
}
