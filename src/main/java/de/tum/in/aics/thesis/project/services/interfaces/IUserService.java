package de.tum.in.aics.thesis.project.services.interfaces;

import java.util.Map;


public interface IUserService {

	public Map< String, Integer> scalePreferences(Map< String, Integer> preferences);
	public Map< String, Integer> sortPreferences(Map< String, Integer> preferences);
}
