package de.tum.in.aics.thesis.project.services.implementations;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import de.tum.in.aics.thesis.project.comparators.PreferencesComparator;
import de.tum.in.aics.thesis.project.daos.interfaces.UserDao;
import de.tum.in.aics.thesis.project.services.interfaces.IPlacesService;
import de.tum.in.aics.thesis.project.services.interfaces.IUserService;

@Service
public class UserServiceImpl implements IUserService {
	private static final int SCALING_FACTOR = 2;
	
	@Autowired
	private UserDao userDao;

	@Autowired
	private IPlacesService placesService;

	@Override
	public Map<String, Integer> scalePreferences(Map<String, Integer> preferences) {	
		for (Entry<String, Integer> entry : preferences.entrySet())	{
		    entry.setValue(entry.getValue() * SCALING_FACTOR);
		}
		return preferences;
	}

	@Override
	public Map<String, Integer> sortPreferences(final Map<String, Integer> preferences) {
		PreferencesComparator comparator = new PreferencesComparator(preferences);
		Map<String, Integer> sortedPreferences = new TreeMap<String, Integer>(comparator);
		sortedPreferences.putAll(preferences);
		return sortedPreferences;
	}
}
