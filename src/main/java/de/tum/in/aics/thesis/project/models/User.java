package de.tum.in.aics.thesis.project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {
	
	private Integer userId;
	private String lastName;
	private String firstName;	
	private String email;
	
	public User() {
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "USER_ID", unique = true, nullable = false)
	public Integer getUserId() {
		return this.userId;
	}
	
	public Integer setUserId(Integer userId) {
		return this.userId = userId;
	}
	
	
	@Column(name = "LAST_NAME", nullable = false, length = 10)
	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	@Column(name = "FIRST_NAME", nullable = false, length = 10)
	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "EMAIL", nullable = false, length = 50)
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

}
