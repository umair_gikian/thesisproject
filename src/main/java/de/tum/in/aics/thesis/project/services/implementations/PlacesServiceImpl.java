package de.tum.in.aics.thesis.project.services.implementations;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import de.tum.in.aics.thesis.project.comparators.PlacesComparator;
import de.tum.in.aics.thesis.project.models.Place;
import de.tum.in.aics.thesis.project.services.interfaces.IPlacesService;

@Component
public class PlacesServiceImpl implements IPlacesService {

	
	
	public Map< String, Map< String, Integer >> categorize(List<Place> places){
	
		Map< String, Map< String, Integer >> categorisedPlaces = new HashMap< String, Map< String, Integer >>();
		Map<String, Integer> museums = new HashMap<String, Integer>();
		Map<String, Integer> nightlife = new HashMap<String, Integer>();
		Map<String, Integer> food = new HashMap<String, Integer>();
		Map<String, Integer> art = new HashMap<String, Integer>();
		Map<String, Integer> nature = new HashMap<String, Integer>();
		Map<String, Integer> music = new HashMap<String, Integer>();
		Map<String, Integer> shopping = new HashMap<String, Integer>();
		Map<String, Integer> sports = new HashMap<String, Integer>();
			
		for (Place place : places) {
			
			  if(place.getTypes().contains("Museum") || place.getTypes().contains("History") || place.getTypes().contains("Historic Site")){
				  museums.put(place.getName(), place.getStats());
				  categorisedPlaces.put("Museum", museums);
			  }
			  
			  if(place.getTypes().contains("Brewery") || place.getTypes().contains("Pub") || place.getTypes().contains("Bar") || 
			  place.getTypes().contains("Club") || place.getTypes().contains("Nightclub")){	
				  nightlife.put(place.getName(), place.getStats());
				  categorisedPlaces.put("Night Life", nightlife);	  
			  }
			  
			  if(place.getTypes().contains("Restaurant") || place.getTypes().contains("cafe") || place.getTypes().contains("Pizza") || 
			  place.getTypes().contains("Coffee Shop") || place.getTypes().contains("Hotel") || place.getTypes().contains("Steakhouse")){
				  food.put(place.getName(), place.getStats());
				  categorisedPlaces.put("Food", food);	  
			  }
			  
			  if(place.getTypes().contains("Art") || place.getTypes().contains("Gallery")){
				  art.put(place.getName(), place.getStats());
				  categorisedPlaces.put("Art", art);
			  }
			  
			  if(place.getTypes().contains("Garden") || place.getTypes().contains("Park")){
				  nature.put(place.getName(), place.getStats());
				  categorisedPlaces.put("Nature", nature);
			  }
			  
			  if(place.getTypes().contains("Entertainment") || place.getTypes().contains("Theater") || place.getTypes().contains("Music") || place.getTypes().contains("Concert")){
				  music.put(place.getName(), place.getStats());
				  categorisedPlaces.put("Music", music);
			  }
			  
			  if(place.getTypes().contains("City") || place.getTypes().contains("Plaza") || place.getTypes().contains("Shopping")){
				  shopping.put(place.getName(), place.getStats());
				  categorisedPlaces.put("Shopping", shopping);
			  }
			  
			  if(place.getTypes().contains("Athletics") || place.getTypes().contains("Sports") || place.getTypes().contains("Playground") || place.getTypes().contains("Soccer")){
				  sports.put(place.getName(), place.getStats());
				  categorisedPlaces.put("Sports", sports);
			  }
		}
		return categorisedPlaces;
	}

	@Override
	public Map<String, Integer> sortPlaces(Map<String, Integer> catgorisedPlaces) {
		PlacesComparator comparator = new PlacesComparator(catgorisedPlaces);
		Map<String, Integer> sortedPlaces = new TreeMap<String, Integer>(comparator);
		if(catgorisedPlaces != null)
			sortedPlaces.putAll(catgorisedPlaces);
		return sortedPlaces;
	}

	@Override
	public Map<String, List<String>> getRecommendedPlaces(Map<String, Integer> sortedUserPreferences, Map< String, Map< String, Integer >> categorisedPlaces) {

		Set<Entry<String, Integer>> setUserPreferences = sortedUserPreferences.entrySet();
		Map<String , Integer> sortedPlaces = new HashMap<String, Integer>();
		List<String> topPlaces = new ArrayList<String>();
		Map<String , List<String>> finalPlaces = new LinkedHashMap<String, List<String>>();
		
		for (Entry<String, Integer> entry : setUserPreferences) {
			sortedPlaces = this.sortPlaces(categorisedPlaces.get(entry.getKey()));
			int counter = 0;
			for (Map.Entry<String, Integer> sortedplace : sortedPlaces.entrySet()) {
				topPlaces.add(sortedplace.getKey());
				counter++;
				if(counter == 5){
					sortedPlaces.clear();
					break;
				}		
			}
			finalPlaces.put(entry.getKey(), new ArrayList<String>(topPlaces));
			topPlaces.clear();
		}	
		return finalPlaces;
	}
}
