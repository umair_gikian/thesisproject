package de.tum.in.aics.thesis.project.models;

public class Location {
	public double lat,lng;
	public Location(double lat,double lng){
		this.lat=lat;
		this.lng=lng;
	}
}
