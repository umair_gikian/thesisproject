package de.tum.in.aics.thesis.project.services.implementations;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import de.tum.in.aics.thesis.project.FourSquare.FourSquarePlaces;
import de.tum.in.aics.thesis.project.models.Location;
import de.tum.in.aics.thesis.project.models.Place;
import de.tum.in.aics.thesis.project.services.interfaces.ISearchService;

@Component
public class SearchServiceImpl  implements ISearchService {
	
	static Location midloc = new Location(0, 0);

	FourSquarePlaces places = new FourSquarePlaces();
	
	public List<Place> search(Location loc1, Location loc2) {
		
		List<Place> allPlaces = new ArrayList<Place>();

		midPoint(loc1, loc2);
		
		int dist = (int) Math.ceil(distance(midloc, loc1, 'K') * 1000);

		String categories = "4d4b7104d754a06370d81259";		
		List<Place> lstPlace = places.search(midloc.lat, midloc.lng, dist,categories, 50);
		mergeList(allPlaces, lstPlace);
		
		categories = "4d4b7105d754a06377d81259";
		List<Place> lstPlace2 = places.search(midloc.lat, midloc.lng, dist,categories, 50);
		mergeList(allPlaces, lstPlace2);
		
		categories = "4d4b7105d754a06373d81259,4d4b7105d754a06376d81259,4d4b7105d754a06374d81259";
		List<Place> lstPlace3 = places.search(midloc.lat, midloc.lng, dist,categories, 50);
		mergeList(allPlaces, lstPlace3);

		return allPlaces;	
	}
	
	@Override
	public List<Place> explore(Location loc1, Location loc2) {
		
		List<Place> explorePlaces = new ArrayList<Place>();

		midPoint(loc1, loc2);
		
		int dist = (int) Math.ceil(distance(midloc, loc1, 'K') * 1000);

		List<Place> lstPlace = places.explore(midloc.lat, midloc.lng, dist, 50);
		mergeList(explorePlaces, lstPlace);
		
		List<Place> lstPlace2 = places.explore(midloc.lat, midloc.lng, dist, 50);
		mergeList(explorePlaces, lstPlace2);
		
		List<Place> lstPlace3 = places.explore(midloc.lat, midloc.lng, dist, 50);
		mergeList(explorePlaces, lstPlace3);
		
		return explorePlaces;	
	}

	private void midPoint(Location loc1, Location loc2) {
		double dLon = Math.toRadians(loc2.lng - loc1.lng);
		double lat1 = Math.toRadians(loc1.lat);
		double lat2 = Math.toRadians(loc2.lat);
		double lng1 = Math.toRadians(loc1.lng);

		double Bx = Math.cos(lat2) * Math.cos(dLon);
		double By = Math.cos(lat2) * Math.sin(dLon);
		midloc.lat = Math.atan2(
				Math.sin(lat1) + Math.sin(lat2),
				Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By
						* By));
		midloc.lng = lng1 + Math.atan2(By, Math.cos(lat1) + Bx);
		System.out.println(String
				.format("debug mid.lat is %f and mid.lng is %f", midloc.lat,
						midloc.lng));
		midloc.lat = Math.toDegrees(midloc.lat);
		midloc.lng = Math.toDegrees(midloc.lng);
	}

	private double distance(Location loc1, Location loc2, char unit) {
		double theta = loc1.lng - loc2.lng;
		double dist = Math.sin(deg2rad(loc1.lat)) * Math.sin(deg2rad(loc2.lat))
				+ Math.cos(deg2rad(loc1.lat)) * Math.cos(deg2rad(loc2.lat))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == 'K') {
			dist = dist * 1.609344;
		} else if (unit == 'N') {
			dist = dist * 0.8684;
		}
		return (dist);
	}

	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private static double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}

	private void mergeList(List<Place> allList, List<Place> list) {
		List<Place> tmpList = new ArrayList<Place>();
		for (Place place : list) {
			boolean exist = false;
			for (Place place2 : allList) {
				if (place.getName().equalsIgnoreCase(place2.getName()))
					exist = true;
			}
			if (!exist)
				tmpList.add(place);
		}

		allList.addAll(tmpList);
	}
}
