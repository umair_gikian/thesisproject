package de.tum.in.aics.thesis.project.services.interfaces;

import java.util.List;
import java.util.Map;

import de.tum.in.aics.thesis.project.models.Place;


public interface IPlacesService {

	public Map< String, Map< String, Integer >> categorize(List<Place> places);
	public Map<String, Integer> sortPlaces(Map<String, Integer> catgorisedPlaces);
	public Map<String , List<String>> getRecommendedPlaces(Map< String, Integer> sortedUserPreferences , Map< String, Map< String, Integer >> categorisedPlaces );	

}
